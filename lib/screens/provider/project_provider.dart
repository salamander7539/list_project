import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:project_first/screens/model/model.dart';

abstract class ProjectApiProvider<T> {
  Future<T> getData();
}

class ProjectApiProviderImpl implements ProjectApiProvider {
  final Dio _dio =  Dio();

  @override
  Future getData() async {
    Response response = await _dio.get('http://jsonplaceholder.typicode.com/posts');
    try {
      if (response.statusCode == 200) {
        print('Success');
        return (response.data as List)
            .map((x) => ListData.fromJson(x))
            .toList();
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }

}