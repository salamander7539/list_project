import 'package:project_first/screens/provider/project_provider.dart';

class ProjectApiProviderInject {
  static ProjectApiProvider _projectApiProvider;

  ProjectApiProviderInject._();
  static ProjectApiProvider projectApiProvider() {
    if (_projectApiProvider == null) {
      _projectApiProvider = ProjectApiProviderImpl();
    }
    return _projectApiProvider;
  }
}