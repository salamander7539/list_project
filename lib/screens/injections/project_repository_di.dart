import 'package:project_first/screens/injections/project_provider_di.dart';
import 'package:project_first/screens/repository/project_repository.dart';

class ProjectRepositoryInject {
  static ProjectRepository _projectRepository;

  ProjectRepositoryInject._();

  static ProjectRepository projectRepository() {
    if (_projectRepository == null) {
      _projectRepository = ProjectRepositoryImpl(
        projectApiProvider: ProjectApiProviderInject.projectApiProvider(),
      );
    }
    return _projectRepository;
  }
}