import 'package:flutter/cupertino.dart';
import 'package:project_first/screens/model/model.dart';
import 'package:project_first/screens/provider/project_provider.dart';

abstract class ProjectRepository<T> {
  Future<T> getData();
}

class ProjectRepositoryImpl implements ProjectRepository {

  final ProjectApiProvider projectApiProvider;

  ProjectRepositoryImpl({@required this.projectApiProvider}) : assert(projectApiProvider != null);

  @override
  Future getData() async {
    final List<ListData> listData = await projectApiProvider.getData();
    return listData;
  }
}