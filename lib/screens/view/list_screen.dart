import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_first/screens/bloc/project_bloc.dart';
import 'package:project_first/screens/injections/project_repository_di.dart';
import 'package:project_first/screens/view/list_screen_view.dart';

class ListScreen extends StatelessWidget {
  const ListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectBlocImpl>(
      create: (context) => ProjectBlocImpl(
        projectRepository: ProjectRepositoryInject.projectRepository(),
      ),
      child: ListScreenView(),
    );
  }
}
