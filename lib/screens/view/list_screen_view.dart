import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_rx/src/rx_workers/utils/debouncer.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:project_first/screens/bloc/project_bloc.dart';
import 'package:project_first/screens/model/model.dart';
import 'package:project_first/screens/view/file_screen.dart';
import 'package:project_first/screens/widget/search_widget.dart';

class ListScreenView extends StatefulWidget {
  const ListScreenView({Key key}) : super(key: key);

  @override
  _ListScreenViewState createState() => _ListScreenViewState();
}

class _ListScreenViewState extends State<ListScreenView> {
  TextEditingController controller = new TextEditingController();

  bool visible;

  int _selectedIndex;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<ProjectBlocImpl>(context).add(GetDataEvent());
    visible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 25.0),
        child: Column(
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  child: ListTile(
                    leading: Icon(Icons.search),
                    title: TextField(
                      onChanged: (value) => {
                        BlocProvider.of<ProjectBlocImpl>(context)
                            .add(FilterDataEvent(value)),
                      _selectedIndex = null,
                    },
                      controller: controller,
                      decoration: new InputDecoration(
                          hintText: 'Найти...', border: InputBorder.none),
                    ),
                    trailing: IconButton(
                      icon: new Icon(Icons.cancel),
                      onPressed: () {
                        controller.clear();
                        _selectedIndex = null;
                        BlocProvider.of<ProjectBlocImpl>(context)
                            .add(FilterDataEvent(''));
                      },
                    ),
                  ),
                ),
              ),
            ),
            BlocBuilder<ProjectBlocImpl, ProjectState>(
              // ignore: missing_return
              builder: (context, state) {
                if (state is ProjectDataLoadedState) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: state.listData.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: InkWell(
                            onTap: () => _onSelected(index),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      state.listData[index].title,
                                      style: GoogleFonts.aBeeZee(fontSize: 18.0,),
                                    ),
                                    Visibility(
                                      visible: _selectedIndex == index
                                          ? !visible
                                          : visible,
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 12.0),
                                        child: Text(
                                          state.listData[index].body,
                                          style: GoogleFonts.aBeeZee(fontSize: 16.0, color: Colors.black45),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: _selectedIndex == index
                                          ? !visible
                                          : visible,
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 12.0),
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => FileScreen(
                                                  title: state.listData[index].title,
                                                  text: state.listData[index].body,
                                                ),
                                              ),
                                            );
                                          },
                                          child: Text('ПОДРОБНЕЕ', style: GoogleFonts.aBeeZee(fontSize: 14.0),),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
