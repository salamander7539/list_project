part of 'project_bloc.dart';

abstract class ProjectState extends Equatable {
  const ProjectState();
}

class ProjectInitial extends ProjectState {
  @override
  List<Object> get props => [];
}

class ProjectDataLoadedState extends ProjectState {
  final List<ListData> listData;

  ProjectDataLoadedState({this.listData});

  @override
  // TODO: implement props
  List<Object> get props => [listData];
}

class ProjectErrorState extends ProjectState {
  final String error;

  const ProjectErrorState({this.error});

  @override
  List<Object> get props => [error];
}

