part of 'project_bloc.dart';

abstract class ProjectEvent extends Equatable {
  const ProjectEvent();
}

class GetDataEvent extends ProjectEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class FilterDataEvent extends ProjectEvent {
  final title;

  FilterDataEvent(this.title);

  @override
  // TODO: implement props
  List<Object> get props => [title];

}

class SetError extends ProjectEvent {
  final String error;

  const SetError({this.error});

  @override
  // TODO: implement props
  List<Object> get props => [error];
}
