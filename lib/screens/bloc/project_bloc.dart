import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:project_first/screens/model/model.dart';
import 'package:project_first/screens/repository/project_repository.dart';

part 'project_event.dart';

part 'project_state.dart';

abstract class ProjectBloc extends Bloc<ProjectEvent, ProjectState> {
  ProjectBloc(ProjectState initialState) : super(initialState);
}

class ProjectBlocImpl extends Bloc<ProjectEvent, ProjectState> {
  final ProjectRepository projectRepository;

  ProjectBlocImpl({this.projectRepository}) : assert(projectRepository != null), super(ProjectInitial());

  @override
  Stream<ProjectState> mapEventToState(ProjectEvent event) async* {
    if (event is GetDataEvent) {
      try {
        final List<ListData> listData = await projectRepository.getData();
        yield ProjectDataLoadedState(listData: listData);
      } on Exception catch (e) {
        yield ProjectErrorState(error: e.toString());
      }
    }
    if (event is FilterDataEvent) {
      try {
        final List<ListData> list = await projectRepository.getData();
        final List<ListData> listData = list.where((element) => element.title.toLowerCase().contains(event.title)).toList();
        yield ProjectDataLoadedState(listData: listData);
      } on Exception catch (e) {
        yield ProjectErrorState(error: e.toString());
      }
    }
  }


}
